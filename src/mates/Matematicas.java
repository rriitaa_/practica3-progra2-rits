/*Copyright [2022] [Rita Barragan]
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

        /**
         * Esta clase contiene un metodo para generar el numero pi
         * @author Rita
         * @version 1.0 28/03/2022
         */


package mates;

import java.util.stream.Stream;
import java.util.Random;

public class Matematicas {

                /**
                 * Genera el número pi mediante expresiones lamda
                 * @param pasos Es el numero de intentos para que llegue al numero pi
                 * @return Devuelve una aproximación al número pi
                 */
        public static double generarNumeroPiLamda(long pasos){
				
		
		return Stream.generate(() -> (Math.pow(Math.random(),2) +Math.pow(Math.random(),2))).limit(pasos).filter(x-> x<=1).count()*4/(double) pasos;

	}
}

		//Hay que usar Math.random();
		//Stream.generate(() -> (new Random()).nextDouble()).limit(10).forEach(System.out::println);
		//vamos a trabajar sobre el modulo
		//return ----------.filter(_).count()*4/long pasos;
		//genera un stream, de modulos aleatorios
		//haremos el modulo de (x,y) 
		//(int a, int b) -> {return a+b;}
		//()->{return 3.1415}
